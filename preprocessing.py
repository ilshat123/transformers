import os
import string
from pprint import pprint
import random
import pandas as pd
import torch
from sklearn.model_selection import train_test_split
from torch.utils.data.sampler import WeightedRandomSampler
from tqdm import tqdm_notebook
import numpy as np

import config
from texts_clean import clean_text


def replace_empty(lines):
    new_lines = []
    for i, line in enumerate(list(lines)):
        if type(line) != str or str(line) not in string.digits:
            line = '0'
        new_lines.append(line)
    return new_lines


def clean_texts(texts):
    cleaned_train_texts = []
    for tweet_text in texts:
        cleaned_text = clean_text(tweet_text).lower()
        split_cleaned_text = cleaned_text.split()
        cleaned_train_texts.append(" ".join(split_cleaned_text))
    return cleaned_train_texts


def create_df(df, test=False):
    if test:
        new_df = pd.DataFrame({
            'id': list(range(len(df) - 1)),
            'text': clean_texts(df[1][1:])
        })
    else:
        new_df = pd.DataFrame({
            'id': list(range(len(df) - 1)),
            'label': replace_empty(df[2][1:]),
            'label_b': ['a'] * len(df[2][1:]),
            'text': clean_texts(df[1][1:])
        })
    return new_df


def create_tsv(file_path, result_file_path, labels_weights=None, replacement=False):
    """
    labels_weights: {'0': 0.8, '1': 0.2}
    Если указан labels_weights, то меняет соотношение элементов классов.
    Например: классы: [0, 0, 0, 1, 0, 1]  labels_weights: {'0': 0.5, '1': 0.5} на выходе перемешанные [0, 0, 1, 1]

    """
    train_df = pd.read_csv(file_path, header=None, sep='\t')

    def sampler(dataset_classes):
        labels_num = {}
        for elem in dataset_classes:
            labels_num[elem] = labels_num.get(elem, 0) + 1
            # считаем количество различных классов
        if labels_weights:
            min_elem = min(labels_weights.values())
            labels_pack = {key: value / min_elem for key, value in labels_weights.items()}
            # получаем коэффициет соотношения для каждого класса
            labels_num2 = {key: labels_num[key] / value for key, value in labels_pack.items()}
            # получаем число элементов для каждого класса
            all_nums = int(min(labels_num2.values()) * sum(labels_weights.values()) / min_elem)
            if replacement is False:
                labels_weight = {key: labels_weights[key] / (labels_num[key] - all_nums * labels_weights[key] / 2) for
                                 key in labels_weights.keys()}
                # Смещаем коэффициент для каждого класса, так как при replacement is False(элементы не повторяются),
                # соотношение будет слишком быстро съезжать и итоговое соотношение получится неверным
                # (например: 2 класса: 4000 и 400 элем, если после того как возьмем 200 элементов из каждого класса
                # не поменяем коэффициенты каждого класса, то первый класс будет выбираться с большей вероятностью,
                # поэтому для меньшего класса заранее завышаем коэффициент.
                # Но это приведет к тому что вначале будет больше элементов второго класса, а под конец их будет меньше
                # всего, хотя общее соотношение сохранится. Поэтому в конце мы еще раз перемешаем классы
                # random.shuffle(classes_ind)

            else:
                labels_weight = {key: labels_weights[key] / (labels_num[key]) for
                                 key in labels_weights.keys()}
                # так как элементы каждого класса не убавляются, то смещение коэффициента не нужно
            classes_weight = []
            for elem in dataset_classes:
                if elem in labels_weights:
                    classes_weight.append(labels_weight[elem])
                else:
                    classes_weight.append(0)
            classes_ind = WeightedRandomSampler(classes_weight, all_nums, replacement=replacement)
            classes_ind = list(classes_ind)
            random.shuffle(classes_ind)
        else:
            # не меняем соотношение классов
            classes_ind = list(range(len(dataset_classes)))
            random.shuffle(classes_ind)
        return classes_ind

    r = sampler(train_df[2])
    train_df = train_df.loc[r]
    test = True if 'test' in result_file_path else False
    new_train_df = create_df(train_df, test)
    new_train_df.to_csv(result_file_path, sep='\t', index=False, header=False)
    if test:
        new_train_df = create_df(train_df)
        new_train_df.to_csv(config.test_tsv_with_labels, sep='\t', index=False, header=False)


def main(target_files_list, result_files_list, prefix_path, preprocessing_folder_path, labels_weights=None):
    """
    Предобработка данных
    :param target_files_list: Названия входных файлов
    :param result_files_list: Названия выходных файлов
    :param prefix_path: Папка для входных файлов
    :param preprocessing_folder_path: Папка для выходных файлов
    :return:
    """
    target_files = [os.path.join(prefix_path, elem) for elem in target_files_list]
    result_files = [os.path.join(preprocessing_folder_path, elem) for elem in result_files_list]

    for targ_file, result_file in zip(target_files, result_files):
        if 'test' in result_file:
            create_tsv(targ_file, result_file, labels_weights=False)
        else:
            create_tsv(targ_file, result_file, labels_weights=labels_weights)


if __name__ == '__main__':
    main(config.target_files_list, config.result_files_list, config.prefix, config.preprocessing_folder,
         labels_weights=config.labels_weights)
