import os

prefix = 'RuADReCT_raw/'
preprocessing_folder = 'preprocessing/'
target_files_list = ['task2_ru_training_raw.tsv', 'task2_ru_validation_raw.tsv', 'task2_ru_test_raw.tsv']
result_files_list = ['train.tsv', 'dev.tsv', 'test.tsv']

train_tsv, dev_tsv, test_tsv = [os.path.join(preprocessing_folder, elem)
                                for elem in result_files_list]

test_tsv_with_labels = os.path.join(preprocessing_folder, 'labels_t.tsv')
# тестовый файл вместе с классами
labels_weights = {'0': 0.5, '1': 0.5}
# соотношение классов

model_name = 'distilbert-base-uncased'

