import dataclasses
import logging
import os
from pprint import pprint

import pandas as pd
import sys
from dataclasses import dataclass, field
from typing import Dict, Optional

import numpy as np

from transformers import AutoConfig, AutoModelForSequenceClassification, AutoTokenizer, EvalPrediction
from transformers import GlueDataTrainingArguments as DataTrainingArguments
from transformers import (
    HfArgumentParser,
    Trainer,
    TrainingArguments,
    glue_compute_metrics,
    glue_output_modes,
    glue_tasks_num_labels,
    set_seed,
)

from transformers import GlueDataset
from sklearn.metrics import precision_recall_fscore_support

import config


@dataclass
class ModelArguments:
    """
    Arguments pertaining to which model/config/tokenizer we are going to fine-tune from.
    """

    model_name_or_path: str = field(
        metadata={"help": "Path to pretrained model or model identifier from huggingface.co/models"}
    )
    config_name: Optional[str] = field(
        default=None, metadata={"help": "Pretrained config name or path if not the same as model_name"}
    )
    tokenizer_name: Optional[str] = field(
        default=None, metadata={"help": "Pretrained tokenizer name or path if not the same as model_name"}
    )
    cache_dir: Optional[str] = field(
        default=None, metadata={"help": "Where do you want to store the pretrained models downloaded from s3"}
    )


def get_num_labels(data_path, train_file_name):
    """Количество классов"""
    path = os.path.join(data_path, train_file_name)
    tsv_file = pd.read_csv(path, header=None, sep='\t')
    labels = list(set(tsv_file[1][1:]))
    return len(labels)


def create_trainer(data_path, train_file_name, model_name):
    def compute_metrics(p: EvalPrediction) -> Dict:
        preds = np.argmax(p.predictions, axis=1)
        return glue_compute_metrics(data_args.task_name, preds, p.label_ids)

    data_args = DataTrainingArguments(task_name="cola", data_dir=data_path)
    training_args = TrainingArguments(
        output_dir="./models/model_name",
        overwrite_output_dir=True,
        do_train=True,
        do_eval=True,
        per_gpu_train_batch_size=32,
        per_gpu_eval_batch_size=128,
        num_train_epochs=10,
        logging_steps=500,
        logging_first_step=True,
        save_steps=1000,
        evaluate_during_training=True,
    )
    set_seed(training_args.seed)
    num_labels = get_num_labels(data_path, train_file_name)
    config = AutoConfig.from_pretrained(
        model_name,
        num_labels=num_labels,
        finetuning_task=data_args.task_name,
    )
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    model = AutoModelForSequenceClassification.from_pretrained(model_name, config=config)
    # Получили готовые модели

    train_dataset = GlueDataset(data_args, tokenizer=tokenizer, limit_length=100_000)
    test_dataset = GlueDataset(data_args, tokenizer=tokenizer, mode='test')
    eval_dataset = GlueDataset(data_args, tokenizer=tokenizer, mode='dev')
    # Подготовили датасеты, используем измененный GlueDataset

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=train_dataset,
        eval_dataset=eval_dataset,
        compute_metrics=compute_metrics,
    )

    return trainer, [train_dataset, eval_dataset, test_dataset]


def get_presicion_recall_f1(y_pred, y_test):
    """
    f1, presicion, recall по каждому классу и общее
    """
    unique_classes = sorted(list(set(y_test)))
    res = precision_recall_fscore_support(y_test, y_pred, average=None, labels=unique_classes)
    results = {}
    for i in range(len(unique_classes)):
        results[unique_classes[i]] = [elem[i] for elem in res]
    res = precision_recall_fscore_support(y_test, y_pred, average='macro')
    results['all'] = res
    return results


def print_presicion_recall_f1(dict_file):
    lines = []
    column_names = ['class', 'precision', 'recall', 'f1', 'count']
    for key, value in dict_file.items():
        lines.append([key, *value])

    from tabulate import tabulate
    print(tabulate(lines, headers=column_names))


def remove_cached_files():
    files = os.listdir(config.preprocessing_folder)
    files = [elem for elem in files if elem.startswith("cached")]
    for file in files:
        os.remove(os.path.join(config.preprocessing_folder, file))


def predict_model(trainer, test_dataset):
    """
    :param trainer: Обученная модель
    :param test_dataset: GlueDataset
    :return:
    """
    predict_res = trainer.predict(test_dataset)
    # Проверили на тестовой выборке
    y_pred = np.argmax(predict_res.predictions, axis=1)
    test_df = pd.read_csv(config.test_tsv_with_labels, header=None,
                          sep='\t')
    true_labels = test_df[1]
    res = get_presicion_recall_f1(y_pred, true_labels[1:])
    # f1, presicion, recall по каждому классу и общее
    return res


def main(data_path, train_file_name, model_name):
    """
    :param data_path: Директория train/dev/test файлов
    :param train_file_name: Имя train файла
    :param model_name: Имя модели
    :return:
    """
    trainer, datasets = create_trainer(data_path, train_file_name, model_name)
    train_dataset, eval_dataset, test_dataset = datasets
    # Создали тренировочную модель, и подготовили датасеты
    trainer.train()
    res = predict_model(trainer, test_dataset)
    return res


if __name__ == '__main__':
    remove_cached_files()
    # удаляем старые кэш файлы
    logging.basicConfig(level=logging.INFO)
    data_path = config.preprocessing_folder
    train_file_name = config.result_files_list[0]
    model_n = config.model_name
    res = main(data_path, train_file_name, model_n)
    print_presicion_recall_f1(res)
