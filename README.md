## Установка
  pip3 install -r requirements.txt

## Использование
### Препроцессинг
  Файл: preprocessing.py
  
  - Порядок работы:
      - Чистит все неподходящие символы с помощью texts_clean.py -> clean_text()
      - Смешивает классы с указанным в config.py(параметр: labels_weights) соотношением
  
  - Папка с исходными файлами: 
      - config.py(параметр: prefix)
  - Папка с предобработанными данными: 
      - config.py(параметр: preprocessing_folder)
  
### Обучение
  Файл: main.py(main()).
  
  - Параметры: 
      - Имя модели: config.py(параметр: model_name)
  
### Оценка
  Оценка происходит сразу после обучения(метод: predict_model).
  Результаты выводятся с помощью метода print_f1_presicion_recall().

## Скрипты
  - Препроцессинг: 
      - preprocessing.py
  - Обучение: 
      - main.py
  - Конфиг: 
      - config.py
  